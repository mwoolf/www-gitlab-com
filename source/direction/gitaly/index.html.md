---
layout: markdown_page
title: "Category Direction - Gitaly"
description: "Gitaly is a Git RPC service for handling all the Git calls made by GitLab. Find more information here!"
canonical_path: "/direction/gitaly/"
---

- TOC
{:toc}

## Gitaly

| | |
| --- | --- |
| Section | [Core Platform](/direction/core_platform) |
| Maturity | Non-marketable |
| Content Last Reviewed | 2024-03-04 |

### Introduction and how you can help

The Gitaly direction page belongs to the [Systems Stage](/handbook/product/categories/#systems-stage) within the [Core Platform](/handbook/product/categories/#core-platform-section)
section, and is maintained by [Mark Wood](https://gitlab.com/mjwood). The Gitaly Engineering team and stable counterparts can be found on the [Engineering team page](https://about.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/gitaly/).

This strategy is a work in progress, and everyone can contribute.
Please comment and contribute in the linked issues and epics.
Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Agitaly)
- [Epic list](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=group%3A%3Agitaly)

If you would like support from the Gitaly team, please see the team's page detailing [How to contact the Gitaly team](https://about.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/gitaly/#how-to-contact-the-team).

### Gitaly Overview

Gitaly is the service responsible for the storage and maintenance of all Git repositories in GitLab for both our GitLab.com customers, as well as self-managed customers.
Git repositories are essential to GitLab, for [Source Code Management](/direction/create/source_code_management/), [Wikis](/direction/plan/knowledge/wiki/), [Snippets](/direction/create/source_code_management/source_code_management/), Design Management, and [Web IDE](/direction/create/ide/web_ide/). Every stage of the DevOps lifecycle to the right of Create - Verify, Package, Release, Configure, Govern, Monitor and Secure - depends on the project repositories. Because the majority of GitLab capabilities depend on information stored in Git repositories, performance and availability are of primary importance.

GitLab is used to store Git repositories by small teams of a few people all the way up to large enterprises with many terabytes of data. For this reason, Gitaly has been built to scale from small single server GitLab instances, to large high availability architectures.

Gitaly provides multiple interfaces to read and write Git data:

- Git protocol over SSH, through the [GitLab Shell](https://gitlab.com/gitlab-org/gitlab-shell) component.
- Git protocol over HTTP, through the [GitLab Workhorse](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/workhorse/index.md) component.
- gRPC internally to GitLab components. The public REST and GraphQL APIs to Git data are implemented using these RPCs.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/RNLWrvIkB9E" frameborder="0" allowfullscreen="true"></iframe>
</figure>
<!-- blank line -->

#### Gitaly within GitLab

While GitLab is the largest user of the Gitaly project, it is important to note that Gitaly is a standalone project that can be adopted separately from GitLab. As such, we strive to ensure that all business specific decisions are made within the GitLab application. Our belief is that Gitaly should provide the ability for management interfaces, but not make any specific management decisions.

For example, some users may want the ability to move repositories between different storage nodes for either cost savings or performance reasons. While Gitaly should provide an easy to use interface to efficiently move repositories, the calling application should be making the decisions around which repositories to move where.

Processes requiring no business data or inputs should be fully contained within Gitaly. These types of processes include repository maintenance and storage maintenance type tasks. We believe that these types of features provide substantial value for projects utilizing Gitaly and provide a compelling reason to chose Gitaly as a repository storage architecture.

#### Gitaly Development

The Gitaly team is divided into two sub-teams which both strive to provide scalable and performant Git repository storage. These two teams are the Git team and the Cluster team, and represent two distinct focuses for the Gitaly group. (For additional details, see the [team page](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/gitaly/).

__Git Team__

Gitaly is built on top of the open source [Git project](https://git-scm.com/), and we feel very strongly that we should be contributing to this project. As users and ambassadors of Git, it is our goal to actively engage in the Git community through bug fixes, feature additions, and mentoring. One of this team's major goals is to ensure we continue to enhance the Git project.

In addition to contributing to upstream Git, the Gitaly Git team is responsible for integrating Git within the Gitaly product. This involves optimizing our internal usage of Git, performing housekeeping on repository data and generally ensuring that the repository storage for our customers is performing as well as possible for all shapes and sizes of data.

__Cluster Team__

The	Cluster team's goal is to provide a durable, performant, and reliable Git storage layer for GitLab. This team will tightly collaborate with the Gitaly Git team as they will build directly upon the Git tooling. Areas where the Cluster team will focus on is High Availability Git storage (Gitaly Cluster), scalability of Gitaly storage mechanisms, and improved user and administrative experiences.

#### Gitaly Cluster

In order to support highly available Git repository storage, [Gitaly Cluster](https://docs.gitlab.com/ee/administration/gitaly/) has been released. This provides redundant storage benefits such as voted writes, read distribution, and data redundancy. For full documentation, please see the details on [Configuring Gitaly Cluster](https://docs.gitlab.com/ee/administration/gitaly/praefect.html).

To fully appreciate the use-case for Gitaly Cluster, we must first clarify the role of highly available repository storage. From the Gitaly perspective, highly available (HA) storage means that a fault-tolerant interface for repository data exists, such that the loss of a single storage node will not compromise the ability to read / write Git data. Gitaly cluster fulfills this role by providing an interface to define multiple Gitaly storage nodes, and set a replication factor for stored repositories (how many nodes each repository should be stored on). In the event of a storage node loss, read and write operations continue as before, and when the cluster is returned to full capacity, the data is re-replicated to the returning node.

What HA repository storage does not provide is improved performance. Though in some cases write performance improves (through read distribution), the general concept is that you are trading storage cost and potential performance impacts for fault tolerance.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them.
-->

The one year plan for Gitaly Group is founded on three main principals discussed below. As Gitaly is one of the foundational elements of GitLab, these principals were chosen ensure that Gitaly is ready to meet future business needs.

### Improve Repository Data Safety

In the unlikely event that there is a repository data issue necessitating in data restoration, we want to ensure that there are sufficient tools in place to allow our customers to be up and running as efficiently and painlessly as possible. To this end, the Gitaly team are actively involved in the [GitLab Disaster Recovery Working Group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/) where we play a critical role in defining opportunities to improve overall repository data recovery time objectives (RTO) and recovery point objectives (RPO). We also meet regularly with customers to better understand our self-managed customer needs.

Our one year focus areas are:

- Improve the user backup experience by designing a point in time backup and restore mechanism
- Apply our learnings from our GitLab Disaster Recovery Working Group to the overall Gitaly product, to improve RTO / RPO for our self-managed customers

### Ensure Gitaly Scales Gracefully

As our customers deploy larger installations of GitLab, they start to hit more pain points around the scalability of Gitaly and its repository management. This is especially true in light of our intent to make cloud-native deployments of Gitaly a reality, where nodes may be spun up and down more regularly.

Our goals here are:

- Provide a better user journey for interacting with very large repositories (in collaboration with the [Source Code group](https://about.gitlab.com/direction/create/source_code_management/))
- Collaborate closely with our [Tenant Scale](https://about.gitlab.com/direction/core_platform/tenant-scale/) team to keep scalable repository management at the forefront of our scalability plans

### Improve the User Experience

We recognize that administrators of self-managed instances want to more easily configure and manage repository storage. In order to assist in both of these user journeys, we plan to do the following:

- Develop a cloud-native approach for Gitaly storage, though collaboration with our [Distribution group](https://about.gitlab.com/direction/distribution/).
- Improve our Gitaly Cluster offering to be more user friendly, and easier to manage
- Become a thought leader in the Git space, providing insights and native GitLab methodologies to perform common tasks in an easy to understand way.

### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

Over the next quarter, the Gitaly team is focused on the following areas. While this is not an exhaustive list, it does give some insight into our major focus areas.

#### Reliable and Scalable Cluster Replication

As our customers continue to grow their repositories (both in count and in size), it is critical that the underlying Gitaly services can scale appropriately to meet these demands. As a team, we have decided that the solution to several of the existing scalability issues surrounding Gitaly cluster is a paradigm shift in how we replicate Git data. As such, we have begun initial efforts in shifting to a [decentralized architecture for Gitaly Cluster](https://gitlab.com/groups/gitlab-org/-/epics/8903).

The highlights of this forward thinking approach are as follows:

- The elimination of the Gitaly PostgreSQL database (Praefect) including the removal of the existing replication queue. This means that there is no more need to work around the single point of failure of this database by using external PostgreSQL redundancy solutions.
- Strong consistency guarantees, including for object pools
- Easier management for data migrations and node management
- Elimination of the difference between Gitaly and Gitaly Cluster. The resultant implementation can simply have a single Gitaly node or multiple Gitaly nodes configured to replicate data.

#### Backups and disaster recovery

While this is not an area we own as a team, we believe that it is crucial for us to support the teams working in these areas to ensure that Git backups and data management allows for rapid and accurate restoration of Git data. The team is heavily involved in leading the [GitLab disaster recovery working group](https://about.gitlab.com/company/team/structure/working-groups/disaster-recovery/). As leaders in this working group, we're collaborating with a broad cross-functional team to help ensure our service's success long-term. As part of this effort, we're architecting a solution for [implementing write-ahead logging](https://gitlab.com/groups/gitlab-org/-/epics/8911) on GitLab.com.

#### Cloud Native Gitaly

We are in the beginning stages of investigating the [known limitations](https://gitlab.com/groups/gitlab-org/-/epics/6127#known-limitations) around Gitaly running well in Kubernetes. This is going to be an ongoing theme across the next year, but we wanted to begin by ensuring that we understood first and foremost what issues exist today. The team is taking a data driven approach involving load testing in an effort to ensure we have clearly identified functional gaps.

<!-- ### What we recently completed -->
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

In order to best represent our [Transparency Value](https://about.gitlab.com/handbook/values/#transparency), it is just as important to clarify what the Gitaly team cannot prioritize currently. This does not mean that we do not recognize the need for some of these features, simply that we have a finite team.

- Better Support for Administrative User Journeys

    We want to ensure that in the future, we support user journeys such as adding,
    removing, and replacing nodes cleanly, and provide a basic administrative dashboard to monitor node health.

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

The version control systems market is expected to be valued at close to US$550mn in the year 2021 and is estimated to reach US$971.8md by 2027 according to [Future Market Insights](https://www.futuremarketinsights.com/reports/version-control-systems-market) which is broadly consistent with revenue estimates of GitHub ([$250mn ARR](https://www.owler.com/company/github)) and Perforce ([$130mn ARR](https://www.owler.com/company/perforce)). The opportunity for GitLab to grow with the market, and grow it's share of the version control market is significant.

Git is the market leading version control system, demonstrated by the [2018 Stack Overflow Developer Survey](https://insights.stackoverflow.com/survey/2018/#work-_-version-control) where over 88% of respondents use Git. Although there are alternatives to Git, Git remains dominant in open source software, usage by developers continues to grow, it installed by default on macOS and Linux, and the project itself continues to adapt to meet the needs of larger projects and enterprise customers who are adopting Git, like the Microsoft Windows project.

According to a [2016 Bitrise survey](https://blog.bitrise.io/state-of-app-development-2016#self-hosted) of mobile app developers, 62% of apps hosted by SaaS provider were hosted in GitHub, and 95% of apps are hosted in by a SaaS provider. These numbers provide an incomplete view of the industry, but broadly represent the large opportunity for growth in SaaS hosting on GitLab.com, and in self hosted where GitLab is already very successful.

#### Key Capabilities
<!-- For this product area, these are the capabilities a best-in-class solution should provide -->

**Support large repositories**

As applications mature, the existing code base continues to grow. As such, average repository sizes are on the rise and version control systems must be able to handle these large repositories in a performant manner. Additionally, many development tasks may require version control of large files, which again, should be handled seamlessly.

**Ensure data safety**

Application code has a very high value to organizations. It is unacceptable to have a solution which does not make it easy to ensure the integrity of your data, as well as provide easy means of backing up and restoring your data should something go wrong. Ideally, these solutions should use efficient and cost effective storage to optimize your business infrastructure.

<!-- #### Roadmap -->
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. -->

#### Top 2 Competitive Solutions
<!-- PMs can choose to highlight a primary BIC competitor--or more, if no single clear winner in the category exists; in this section we should indicate: 1. name of competitive product, 2. links to marketing website and documentation, 3. why we view them as the primary BIC competitor -->

Important competitors are [GitHub.com](https://github.com) and [Perforce](https://perforce.com) which, in relation to Gitaly, compete with GitLab in terms of raw Git performance and support for enormous repositories respectively.

Customers and prospects evaluating GitLab (GitLab.com and self hosted) benchmark GitLab's performance against GitHub.com, including Git performance. The Git performance of GitLab.com for easily benchmarked operations like cloning, fetching and pushing, show that GitLab.com similar to GitHub.com.

Perforce competes with GitLab primarily on its ability to support enormous repositories, either from binary files or monolithic repositories with extremely large numbers of files and history. This competitive advantage comes naturally from its centralized design which means only the files immediately needed by the user are downloaded. Given sufficient support in Git for partial clone, and sufficient performance in GitLab for enormous repositories, existing customers are waiting to migrate to GitLab.

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)

## Git

Git lies at the core of what Gitaly does and is of critical importance to
GitLab. We are aiming to increase up our investment into the Git community to
ensure that it stays healthy in the long term.

Direction for the GitLab contributions to the Git project is tracked separately
from the Gitaly direction, even though they are closely tied to each other.
This is done because not only is Git a standalone project, but more importantly
it is a community-led open source project, and thus not under direct control of
the Gitaly team.

While we can influence the direction of Git somewhat by being regular
contributors to the project and building trust and credit with the community,
it will never be (and should not be!) under our full control. This is only
natural in an open source project and also by design, as the success of Git
relies on a healthy and diverse community with interests that may at times be
conflicting with each other. We want to foster that community and collaborate
with it, including with our direct competitors.

### Monorepo performance

There's no question, repository data sets are growing rapidly. With the
prevalence of machine learning and artificial intelligence adoptions, we're
seeing a steady increase in repository size as well as the number of files
needing to be stored. The Git team needs to ensure that repository storage
scales with these expanding data storage needs.

Monorepo performance is a complex topic that involves many different parts of
Git.

#### Scaling with many objects

As projects grow older and grow more commits, the repository's object graph
naturally expands. This creates scalability issues over time regardless of the
respective object sizes. We want to ensure that the object database of Git
repositories can scale well when facing repositories with dozens of millions of
objects.

Our goals here are:

- **Pluggable object databases**: The object storage format and the transport
  format of Git are tightly coupled. Furthermore, access to the object format
  happens rather ad-hoc without a lot of abstraction. It is clear though that
  the current design of the object database has limited scalability, and the
  more features we pile on top of it the harder it becomes to continue scaling.

  We need to get back to the drawing board and explore whether it is feasible
  to make the object database pluggable. This is a huge effort and will likely
  span over multiple years, but it is needed to ensure long-term scalability of
  Git.

- **Better support for bundle URIs**: We have started to use bundle URIs to help
  reduce the load on Gitaly servers. This is especially important in the
  context of CI systems. The feature is still relatively young though, and
  there are issues with both the implementation and the UX. We need to invest
  more time to help polish this feature.

#### Scaling with large objects

Despite issues with large object graphs, Git is also known to not scale well
with large objects like binary files. These are becoming increasingly more
important with the advent of AI, but are also critical to open up to additional
markets that are heaviliy dependent on binary assets like game studios.

Our goals are:

- **Offloading blobs through promisor remotes**: We need server side changes to
  enable a triangular workflow between Git server, client, and a promisor
  remote which will host blobs that we offload from the git server.

- **Allow easy access to promisor remotes**: Make it easier for Git to access
  promisor remotes. If we have a lot of objects offloaded to object storage
  because they are rarely used, Git should be able to access the objects and
  metadata easily without having to pull the objects back into the repository.

- **Storing large objects more efficiently**: Investigate whether we can store
  large objects more efficiently and allow for deduplication when such large
  objects are changed. One example would be to use rolling hash functions to
  split up large objects better, which would potentially allow us to store
  changes to such large objects incrementally. Furthermore, if we are able to
  expose those chunks via the transport layer, then this would potentially be
  able to provide a native replacement for Git LFS when combined with partial
  clones.

- **Sending large objects more efficiently**: Next to the problem of storing
  large objects efficiently, we also need to investigate how to send incremental
  changes to large objects over the wire efficiently. This may require us to
  expose the above representation with rolling hash functions to clients to
  efficiently compute deltas. An alternative would be to introduce a new object
  type that allows us to serve large binaries more efficiently.

#### Scaling with many references

We have seen repeatedly that Git and Gitaly start to struggle in repositories
that have millions of references. While one approach should be to reduce the
number of references that we need to store, we also need to acknowledge that
our customers have valid usecases for repositories with this many references.
Adapting Git so that it can handle repositories with many references is thus
important.

Our goals here are:

- **Implement reftable support in GitLab**: Reftables are a new storage data
  structure within Git that we have the privilege of contributing to upstream
  Git. This back end for Git references will improve the performance of
  repositories with many references, as well as eliminate the current race
  condition around updating reference. This will translate into better support
  for monorepos and will help ensure continued stability for our GitLab.com
  offering.

  [Tracking Epic](https://gitlab.com/groups/gitlab-org/-/epics/4220).

- **Performance improvements for the reftable backend**: While the reftable has
  been upstreamed and generally outperforms the files backend in most
  scenarios, there still are lots of performance optimizations feasible. To
  reap the full benefits of this new backend we should invest more time into
  this.

- **Partial-update transactions**: For the reftable backend to operate
  efficiently, updates of multiple references should ideally happen in a single
  transaction. Tools like git-fetch(1) may want to update only a subset of
  those refs though where the expected state matches the actual state of a
  reference. This is done via separate transactions. With the introduction of
  partial-update transactions, we allow evicting references from such a
  transaction, while still continuing with remaining references.

#### Scaling with large trees

Many commands in Git scale with the size of the file tree, and large monorepos
tend to have file trees that are both deep and broad. This creates bottlenecks
in commands that scale with tree size. Adapting those commands to handle such
large trees better is thus another angle for ensuring future scalability.

Our goals here are:

- **Better UI for sparse checkouts (client side)**: Sparse checkouts help users
  scale by not requiring a fully checked out worktree. For one, this saves disk
  space. Second, this allows users to skip fetching many blobs for partial
  clones. The sparse checkout tool in Git is hardly used because of usability
  challenges. We can vastly improve this command to make it more easily usable for
  monorepo cases.

- **git-replay(1)**: Improve git-replay(1) to support additional usecases for
  cherry-picks, rebases, rebasing merges and reverts.

- **git-blame-tree(1)**: Upstream git-blame-tree(1) to improve performance when
  computing the last commit entries in a specific tree have been changed. This
  should speed up RPCs like `ListLastCommitsForTree()` significantly.

### Better support for cloud-native deployments

Many of our customers have expressed a big interest in cloud-native deployments
of Gitaly. Our official stance is that this is not supported, where a huge
contributor to this stance is that Git does not handle constrained environments
well.

Our goals here are:

- **Memory growth**: Start to address memory growth issues in tools like
  git-pack-objects(1) in large monorepos. Hopefully, it shouldn't be required
  to allocate dozens of gigabytes of memory to pack objects. This is especially
  important in the context of monorepos and Kubernetes. It is assumed that this
  will be a moving target, and depending on how much bandwidth we allocate, we
  expect this to take multiple quarters.

### User Experience

Git can be an intimidating tool with very complex syntax. This poses a direct
threat because it allows a contender in the same space to take away market
share if they provide a significantly better user experience. This has been
showing with the recent advent of Jujutsu, which is claimed by many to provide
a superior user experience compared to Git.

Our goals here are:

- **More streamlined interfaces**: The user interfaces provided by Git have
  grown organically over time and are often redundant, inconsistent, confusing
  and/or hard to discover. We want to invest the time to make the interfaces
  more consistent and thus easier to use. We want to help the community to
  adopt a set of guidelines to help adapt existing and design future user
  interfaces.

- **Better user journey with large repositories**: Provide a better user journey
  for interacting with very large repositories (in collaboration with the
  [Source Code group](https://about.gitlab.com/direction/create/source_code_management/)).

- **Observe challengers like Jujutsu**: We want to observe development of
  challengers like [Jujutsu](https://martinvonz.github.io/jj/latest/) (JJ), a
  new version control system funded by Google for improved user experience with
  monorepos. We have noticed that many users praise its usability, especially
  compared to Git. As JJ can use Git repositories as backend, we may want to
  investigate whether it may be a good addition to the toolchain of users.

  Furthermore, we may want to help Google with the libification of Git in this
  context. If the Git backend used by JJ plays well with Git due to this
  effort, then we ensure that JJ will stay compatible with repositories hosted
  on GitLab.

  By staying close to developments in JJ, we position ourselves such that we can
  start to pivot to JJ if this ever becomes necessary.

### Maintainability

As we have started to invest more into the Git community, we need to treat it
with the same focus on maintainability as we treat projects that are owned by
GitLab. We want to ensure that the codebase remains accessible to existing and
new contributors and readily extensible. As we entrust Git with handling our
customer's most important data, we also need to ensure that the Git project is
safe.

Our goals here are:

- **Integer overflows**: During the audit of X41 D-Sec we found many integer
  overflows in Git that can lead to exploitable out-of-bounds writes. The worst
  issues were able to trigger remote code execution bugs in Gitaly. While known
  instances have been fixed, Git is full of implicit integer conversions that
  can lead to this kind of bug. We should invest the time to harden Git against
  this kind of vulnerability and fix this whole class of bugs. This requires
  lots of fixes throughout the codebase so that we can enable relevant compiler
  warnings.

- **Memory leaks**: While many parts of Git are leak free, others aren't. This
  is reflected in Git's test suite, where only a subset of tests run with the
  leak sanitizer enabled. This makes it easy to introduce new memory leaks that
  go unnoticed. The effect is that we are working against a moving target when
  trying to reduce memory usage of Git overall. We want to help the Git project
  such that the whole test suite is free of memory leaks.

- **Libification**: The Git project provides the command line tools as the only
  interface to Git repositories, which makes it hard to embed Git-related
  functionality into applications directly. There are ongoing efforts to also
  provide a linkable library that exposes parts of Git's functionality to such
  applications. This requires a large effort to make Git play nice for such a
  usecases: removal of global state, removing calls to `exit()` and `abort()` and
  general refactorings of the to-be-exposed interfaces. We want to help this
  effort to eventually have a proper Git library, which would also open up the
  ability for ourselves to for example provide long-running Git daemons.

- **Structured error handling**: Improve error handling such that Git can return
  information about the root cause of failures in a structured way. We
  currently have to rely on parsing error messages to figure out what exactly
  has gone wrong, which is fragile and often leads to bugs.

- **Improved tracking of direction** While core developers of the Git community
  are typically aware of ongoing projects, this is less true for the wider
  community. This makes it hard to pick up work as a new contributor, requires
  the community to often re-hash similar discussions, and doesn't allow outside
  parties to learn about the direction. We want to help the Git project to
  track direction better to improve the flow of information.

### What is Not Planned Right Now

- [VFS for Git](https://gitlab.com/groups/gitlab-org/-/epics/93)

    Partial Clone is built-in to Git and available in GitLab 13.0 or newer.
    [Scalar](https://devblogs.microsoft.com/devops/introducing-scalar/) is
    compatible with partial clone, and Microsoft is contributing to its
    improvement based on their learnings from the VFS for Git protocol.

- Divergent solution for CDN Offloading

    While we recognize that a lot of good work has gone into independent
    solutions, we are committed to work with the Git community on a CDN
    approach. We intend to support, implement, and contribute to this
    solution as it be comes available. This is currently being explored
    in our [Support Git CDN offloading](https://gitlab.com/groups/gitlab-org/-/epics/1692)
    epic.

## Analyst Landscape

<!--
What are analysts and/or thought leaders in the space talking about?
What are one or two issues that will help us stay relevant from their perspective?
-->

- [Native support for large files](https://gitlab.com/groups/gitlab-org/-/epics/773) is important to companies that need to version large binary assets, like game studios. These companies primarily use Perforce because Git LFS provides poor experience with complex commands and careful workflows needed to avoid large files entering the repository. GitLab has been supporting work to provide a more native large file workflow based on promiser packfiles which will be very significant to analysts and customers when the feature is ready.

### Sales Enablement

This section contains messaging, questions, and resources for our sales counterparts to successfully position and sell Gitaly Cluster. It is important to note that Gitaly Cluster is not perfect for every installation. Our goal is to provide options for our customers so they can choose the best repository storage mechanism for their particular business needs.

#### What is Gitaly?

Gitaly is a centralized service which handles all access to files to file storage for GitLab. Gitaly services Git requests from the GitLab web application, command line, and via the API. Gitaly is highly configurable and can utilize one or more storage locations to read / write repository data.

The Gitaly service is required for all GitLab installs, and is a separate product from Gitaly Cluster. While Gitaly handles accessing repository storage, Gitaly Cluster provides a highly available repository storage solution for our customers.

#### Why we built Gitaly Cluster

Gitaly Cluster was built to address the industry-wide difficulty around expanding Git repository storage in addition to the lack of high availability (HA) Git storage for critical applications. A prominent theme in industry is the idea of an ever expanding NFS storage location for repository storage. While this can work, over time performance degrades, and management becomes increasingly complex. Additionally, while the NFS file system is ideal for many types of files, it's well documented that the types of file accesses created by Git repository access can cause performance issues.

Our goal with Gitaly cluster is to build a Git repository storage system capable of scaling with our users needs, and providing a configurable level of redundancy to keep businesses operating, iterating, and growing.

#### How is Gitaly Cluster differentiator for GitLab

Gitaly Cluster is a unique open-core project aimed at providing a scalable and high availability platform for Git repository storage. Gitaly Cluster enable horizontal scalability, allowing our customers to grow their storage in a simple, and well defined manner. We also capitalize on the redundant copies of data needed for HA by increasing read performance through read-distribution.

#### When should customers use Gitaly Cluster

Customers should utilize Gitaly Cluster in a few key situations:

- **There is a need for high availability** - Customers who wish to ensure that losing a single node does not induce downtime are ideally suited to a Gitaly Cluster installation.
- **There is a need for expandable storage** - Customers whose repository storage size continues to grow and want to be able to horizontally scale their storage infrastructure.
- **There is a large read demand on Git data** - Environments where there is a large read demand on data can benefit greatly from the distributed read functionality provided by Gitaly Cluster.

#### When should customers not use Gitaly Cluster

Customers may not desire to utilize Gitaly Cluster for the following reasons:

- **Cost** - Having data stored in a highly available / redundant manner requires more infrastructure and therefore incurs higher storage / hosting costs.
- **Very low RPO / RTO needs** - Recognize that restoration of an entire cluster & Praefect database will naturally take longer than a single node. However, this is mostly mitigated by the fact that loss of a single node should not require restoration, and therefore should be an unlikely scenario.
- **Require snapshot backups** - It is very difficult to backup the Gitaly Cluster nodes & Praefect database at the exact same time, which will result in a non-congruent backup. Therefore, Gitaly Cluster does not support snapshot backups. However, we are making progress on releasing an [incremental backup solution](https://gitlab.com/groups/gitlab-org/-/epics/2094) that will be fully compatible with Gitaly Cluster.

#### Resources
**Documentation Resources**
- [Gitaly Cluster Recommendations](https://docs.gitlab.com/ee/administration/configure.html#gitaly-capabilities)
- [Gitaly Cluster documentation](https://docs.gitlab.com/ee/administration/gitaly/)
- [FAQ](https://docs.gitlab.com/ee/administration/gitaly/faq.html)

**Enablement Presentation** (Internal GitLab Only)
- [Video: Gitaly Cluster Enablement Presentation](https://youtu.be/zI1t0IyHayM)
- [Deck: Self-hosted options for GitLab](https://docs.google.com/presentation/d/1RV-dOah-EO4D4VZvSwEVxaXEXbVSlc6FzjZjQIYSre8/edit#slide=id.g29a70c6c35_0_68)

<!--
    TODO:
- Pitch Deck - TBD
-->

### Deprecations and Changes

As Gitaly and Gitaly Cluster evolve, it is sometimes necessary to deprecate features. When this occurs, we will follow the documented [Deprecations, removals and breaking changes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#deprecations-removals-and-breaking-changes) procedure. This ensures that all stable counterparts within GitLab are informed, and that the [GitLab Documentation](https://docs.gitlab.com/ee/update/deprecations) is also updated to keep our customers informed.

In addition, we will track all deprecations throughout the 16.x milestones, and breaking changes occurring in the 17.0 milestone on the following epic:

- [Gitaly 16.x through 17.0 Deprecations and Removals](https://gitlab.com/groups/gitlab-org/-/epics/11037)

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

Gitaly is a **non-marketable** category, and is therefore not assigned a maturity level.

### Target Audience

<!--
An overview of the personas involved in this category.
An overview of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.
-->

**Systems Administrators** directly interact with Gitaly when installing, configuring, and managing a GitLab server, particularly when high availability is a requirement. In the past, systems administrators needed to create and manage an NFS cluster to configure a high availability GitLab instance, and manually manage the failover to new Gitaly nodes mounted on the same NFS cluster. In order to scale such a solution individual storage nodes needed to be re-sized, or a sharded Gitaly approach was required. Now that Gitaly Cluster is available, is possible to eliminate the NFS cluster from architecture and rely on Gitaly for replication. Gitaly Cluster brings with it automatic failover, horizontal scaling, and read access across replicas will deliver 99.999% uptime (five 9's) and improved performance without regular intervention. Systems administrators will have fewer applications to manage, as the last projects are migrated to GitLab and other version control systems are retired.

**Developers** will benefit from increasing performance of repositories of all shapes and sizes, on the command line and in the GitLab application, as performance improvements continue. Once support for monolithic repositories reaches minimal and continues maturing, developers will no longer be split between Git and legacy version control systems, as projects consolidate increasingly on Git. Developers that heavily use binary assets, like **Game Developers**, will at long last be able to switch to Git and eliminate Git LFS by adopting native large file support in Git.
